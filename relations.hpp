#ifndef RELATIONS
#define RELATIONS


#include <variant>
#include <set>
#include <map>
#include <iostream>
#include <typeinfo>
#include <memory>

#include <metal>
#include "blot/blotmeta.hpp"

using namespace std;

// solve_formula( r( a, _1 , b) && s(c, _2))
// should return a list of (_1,_2) that satisfy the formula. 
// should be lazy evaluated.
// should be templated
// solve_expression should do a similar thing, except with no free variables, and return a bool


// each relation should implement a get function
// for instance r.get(a,_1,b) should return a list of _1 that satisfy this relation.
// The implementation depends on the relation, could use a hash table or a function. 
// How to deal with infinite relations?
//
// Some relations can have a r.set(..) function which manually extends the relation
//
// Should run at run-time since the relations may only be built at run-time, although there can be some compile-time operations to make coding easier.
// indexing of hana tuples must be done at compile time.

//implement binary symmetric relations first
//
//is there some kind of multi hash table for the implementation of m-ary relations?
//one option is to use sqlite
//
//auto return type of virtual member functions not allowed in c++
//templated virtual member functions not allowed in c++ (but virtual members allowed in class templates)


enum Var_state { unknown, is, isnot, all, none};

//The variable name, V, is encoded in the type. 
template<class V, class T> 
class Variable{
  T value;
  Var_state vs;
};

template<class E1, class E2>
struct And_expr{
  E1 e1;
  E2 e2;
};

template<class E1, class E2>
struct Or_expr{
  E1 e1;
  E2 e2;
};

template<class E1>
struct Not_expr{
  E1 e1;
};

//the Ts are a parameter pack of Variable or types of the same arity as the relation r
template<class R, class ... Ts>
struct Atomic_expr{
  shared_ptr<R> r;
  std::tuple<Ts...> args;
};

//the Vs are a parameter pack of Variable
template<class ... Vs>
struct Formula_result{
  vector<std::tuple<Vs...>> solutions;
};

//need some helper functions here for the runtime AND operation
//the branching based on whether V is in T1 or T2 can be done at compile time using sfinae
//this can be applied over T3 using parameter pack expansion
// remember that we can't index tuples at runtime
template<class V, class T1, class T2>
helper1(T1 t1, T2 t2){
  get<V>(t1)


//solve_formula returns a Formula_result
template<class E1, class E2>
auto solve_formula(And_expr<E1,E2> e0){
  auto r1 = solve_formula(e0.e1);
  auto r2 = solve_formula(e0.e2);

  using t1 = metal::as_list<r1>;
  using t2 = metal::as_list<r2>;
  using t3 = blotmeta::nub_unique<t1,t2>;

  blotmeta::from_list<Formula_result, t3>::type ans;

  //collect indices of the nub_unique
  using idxs1 = metal::transform< metal::partial<metal::find, t1> , t3>;
  using idxs2 = metal::transform< metal::partial<metal::find, t2> , t3>;

  vector<int> idxs1_rt = to_vec<int, idxs1>;
  vector<int> idxs2_rt = to_vec<int, idxs2>;

  for(auto p1 : r1.solutions){
    for(auto p2 : r2.solutions){

      from_list<tuple, t3> temp;
      for(auto ii=0; ii < std::tuple_size<temp>{} ; ++ii){

        int jj = idxs1_rt[ii];
        int kk = idxs2_rt[ii];




}



//vector<hana::tuple<Xs...>> get (Ys...) // where Xs, Ys are template parameter packs for the search and constraint types.
//but how to treat the case where there are no Xs ?
/*
template<class T1, class T2> 
class binary_relation{
    //set<T1> universe1; 
    //set<T2> universe2; 
    map<T1,set<T2>> hash1;
    map<T2,set<T1>> hash2;
    
  public:
    void setr(T1 x1, T2 x2){ 
      hash1[x1].insert(x2);
      hash2[x2].insert(x1);
    };

    template<class TT1, class TT2>
    auto operator() (TT1 x1, TT2 x2);
}

//we want:
//vector<T1> r( _ , x2)
//vector<T2> r( x1 , _)
//vector< pair<T1,T2>> r( _ , _)
//bool r(x1, x2)
//
//This can be done with overloading for simple cases, but maybe we should use hana for more complex ones

template<class T1, class T2> 
template<class TT1, class TT2>
auto binary_relation::operator() (TT1

*/

#endif

#ifndef UNITS
#define UNITS


const double hbarSI = 1.05457173e-34;
const double elemSI = 1.60217657e-19;
const double eps0SI = 8.85418782e-12;
const double angSI = 1.0e-10; //angstrom
const double invangSI = 1.0e10; //inverse angstrom
const double meSI = 9.10938291e-31; // electron mass
const double kbSI = 1.3806488e-23; // boltzmann constant
const double clightSI = 3.0e8; // speed of light
const double amuSI = 1.66053892e-27; //atomic mass unit

// useful units
                                                  
const double bohr2a = 0.52917721092;  //1 bohr in units of ang
const double hart2ev = 27.2113850560; //1 Hartree in units of eV
const double ryd2ev = 13.60569253; //1 Rydberg in units of eV
const double cm12ev = 0.1241/1000;
const double mubevt = 5.7883818066e-5; // bohr magneton, [eV/T]

//gaussian cgs units
const double cCGS = 2.99792458e10;
const double elemCGS = 4.80320425e-10; // charge of electron in cgs units
const double hbarCGS = 1.05457266e-27;
const double evCGS = 1.6021772e-12; // one eV in cgs units
const double angCGS = 1.0e-8; //angstrom
const double invangCGS = 1.0e8; //inverse angstrom
const double meCGS = 9.10938215e-28; // electron mass
const double kbCGS = 1.3806488e-16; // boltzmann constant




#endif

#ifndef LOOKUPFORMULA
#define LOOKUPFORMULA


#include <vector>
#include <tuple>
#include <iostream>
#include <fstream>
#include <string>
#include <blot/metal/include/metal.hpp>

// implementation of lookup tables when the relation between index column and the other columns can be given by a formula.
// Assumes that this formula is a bijection between the index set and the cartesian product of other column sets.

template<class seq1, class seq2>
using remove_list = metal::remove_if<seq1, metal::partial<metal::lambda<metal::contains>,seq2>>;

template<class SI_formula, class MI_formula,  class J, class ...T>
struct lookupformula{
  SI_formula si_formula; //J SI_forumla::operator() (std::tuple<T...>)
  MI_formula mi_formula; //std::tuple<T...> MI_forumla::operator() (J)

  const std::vector<J> s_label;
  const std::tuple<std::vector<T>...> m_label;
  lookupformula(SI_formula s_f, MI_formula m_f, std::vector<J> s_l, std::tuple<std::vector<T>...>  m_l );

  template<class ...H>
  std::vector<J> lookup_idx (H... h) const;

  template<class G>
  G lookup_col (J j) const;

  void check_inverse() const;

  private:

  template<class H1, class ...H>
  void arg_setter (std::tuple<T...>& argfixed, H1 h1, H... h) const;

  template<class H1>
  void arg_setter (std::tuple<T...>& argfixed, H1 h1) const;

  
};

template<class Glist, class SI_formula, class MI_formula,  class J, class ...T>
struct arg_looper{
  std::vector<std::tuple<T...>> func (std::tuple<std::vector<T>...> m_l, std::vector<std::tuple<T...>> args) const;
};
 



template<class SI_formula, class MI_formula, class J, class ...T>
lookupformula<SI_formula, MI_formula, J, T...>::lookupformula(SI_formula s_f, MI_formula m_f, std::vector<J> s_l, std::tuple<std::vector<T>...> m_l ) :
  mi_formula(m_f), si_formula(s_f), s_label(s_l), m_label(m_l) { }

template<class SI_formula, class MI_formula, class J, class ...T>
template<class ...H>
std::vector<J> 
lookupformula<SI_formula, MI_formula, J, T...>::lookup_idx(H...  h ) const{
  std::vector<J>  ans;

  auto arg0 = mi_formula(s_label[0]);
  arg_setter(arg0, h...);
  using Glist = remove_list< metal::list<T...>, metal::list<H...>> ;
 
  std::vector<std::tuple<T...>> args;
  args.push_back(arg0);
  //using AL = metal::apply< metal::lambda<lookupformula<SI_formula, MI_formula, J, T...>::arg_looper>, Glist>;
  //using AL = lookupformula<SI_formula, MI_formula, J, T...>::template arg_looper<Glist>;
  using AL = arg_looper<Glist,SI_formula, MI_formula, J, T...>;
  AL al;
  auto args1 = al.func(m_label,args); 

  for (auto a: args1){
    ans.push_back(si_formula(a));
  }

  return ans;
}


template<class SI_formula, class MI_formula, class J, class ...T>
template<class H1, class ...H>
void 
lookupformula<SI_formula, MI_formula, J, T...> ::
arg_setter (std::tuple<T...>& argfixed, H1 h1, H... h) const{
  std::get<H1>(argfixed) = h1;
}

template<class SI_formula, class MI_formula, class J, class ...T>
template<class H1>
void 
lookupformula<SI_formula, MI_formula, J, T...> ::
arg_setter (std::tuple<T...>& argfixed, H1 h1) const{
  std::get<H1>(argfixed) = h1;
}

template<class Glist, class SI_formula, class MI_formula, class J, class ...T>
std::vector<std::tuple<T...>>
arg_looper<Glist, SI_formula, MI_formula, J, T...> ::func (std::tuple<std::vector<T>...> m_l, std::vector<std::tuple<T...>> args) const {

  using Glistlen = metal::size<Glist>;

  if constexpr (std::is_same<Glistlen, metal::number<0> >::value){
    return args;
  }
  else{

    using Glisthead = metal::front<Glist>;
    using Gtail = metal::drop<Glist, metal::number<1>>;
    std::vector<std::tuple<T...>> args1;
    for (auto a: args){
      for (auto g : std::get< std::vector<Glisthead> >(m_l)){
        std::get<Glisthead>(a) = g;
        args1.push_back(a);
      }
    }

    using AL = arg_looper<Gtail,SI_formula, MI_formula, J, T...>;
    AL al;
    return al.func(m_l,args1); 
  }
}


template<class SI_formula, class MI_formula, class J, class ...T>
template<class G>
G lookupformula<SI_formula, MI_formula, J, T...>::lookup_col (J j) const {
  auto m = mi_formula(j);
  G g = std::get<G>(m); 
  return g;
}


template<class SI_formula, class MI_formula, class J, class ...T>
void lookupformula<SI_formula, MI_formula, J, T...> :: check_inverse () const {

  for (auto s : s_label){
    auto m = mi_formula(s);
    auto s1 = si_formula(m);
    if (s!=s1){ 
      std::cout << "si_formula and mi_formula are not inverses" << std::endl;
      std::cout << s.value << std::endl;
      std::apply([](auto&&... aaa) {((std::cout << aaa.value << " "), ...);}, m);
      std::cout << std::endl;
    }
  }
} 


#endif
